package com.vti.springframework.controller;

import com.vti.springframework.modal.dto.AccountRequest;
import com.vti.springframework.modal.dto.CreateAccountDto;
import com.vti.springframework.modal.entity.Account;
import com.vti.springframework.service.impl.AccountService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/account")
@CrossOrigin("*")
public class AccountController {
    @Autowired
    private AccountService service;

    @PostMapping("/search")
    public Page<Account> all(@RequestBody AccountRequest request){

        return service.getAllAccount(request);
    }

    @GetMapping("/search")
    public List<Account> getAllAccount1(@RequestParam String username, String email){
        return service.searchByName(username,email);
    }

    //Dung pathvariable
    @GetMapping("/{id}")
    public Account getByIdV1(@PathVariable(name = "id") int id){
        return service.getByID(id);
    }

    //Dùng requestparam
    @GetMapping("/get")
    public Account getByIdV2(@RequestParam(name = "id") int id){
        return service.getByID(id);
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable(name = "id") int id){
        try{
            service.delete(id);
            return "xóa thành công";
        }catch (EmptyResultDataAccessException e){
            return "Account không tồn tại!!!";
        }catch (Exception e){
            return "Có lỗi xảy ra!!!";
        }
    }

    @PostMapping("/create")
    public String create(@RequestBody CreateAccountDto accountDto){
        Account account = new Account();
//        Convert từ DTO sang entity
//        Cách 1:
//        account.setEmail(accountDto.getEmail());
//        account.setUsername(accountDto.getUsername());
//        account.setPassword(accountDto.getPassword());

        //Cách 2: dùng BeanUtils
        BeanUtils.copyProperties(accountDto,account);
        System.out.println(account);

        try{
            service.create(account);
            return "Thêm mới thành công";
        }catch (Exception e){
            return e.getMessage();
        }
    }

    @PutMapping("/update/{id}")
    public String update(@PathVariable int id ,@RequestBody CreateAccountDto accountDto){
        Account account = new Account();

        BeanUtils.copyProperties(accountDto,account);
        account.setId(id);

        try{
            service.update(id,account);
            return "update thành công";
        }catch (Exception e){
            return e.getMessage();
        }
    }
}
