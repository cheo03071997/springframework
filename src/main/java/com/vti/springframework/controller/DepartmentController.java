package com.vti.springframework.controller;

import com.vti.springframework.modal.dto.DepartmentRequest;
import com.vti.springframework.modal.entity.Department;
import com.vti.springframework.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/department")
@CrossOrigin("*")
public class DepartmentController {
    private final IDepartmentService service;

    @Autowired
    public DepartmentController(IDepartmentService service) {
        this.service = service;
    }

    @GetMapping("/get-all")
    public List<Department> getAllDepartment(){
        return service.getAll();
    }

    @PostMapping("/search")
    public List<Department> search(@RequestBody DepartmentRequest request){
        return service.search(request);
    }

    @GetMapping("/{id}")
    public Department getById(@PathVariable(name = "id") int id){
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable(name = "id") int id){
        try{
            service.delete(id);
            return "Xóa thành công";
        }catch (Exception e){
            return "Department không tồn tại!!!";
        }
    }
}
