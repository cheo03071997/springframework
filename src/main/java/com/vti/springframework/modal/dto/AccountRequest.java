package com.vti.springframework.modal.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountRequest {
    private String name;
    private String role;
    private String department;

    private int pageSize;
    private int pageNumber;
    private String sortBy;
    private String sortType;
}
