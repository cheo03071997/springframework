package com.vti.springframework.modal.entity;



import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "`Account`")
@Data
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`")
    private int id;

    @Column(name = "email", length = 50,unique = true,nullable = false,updatable = false)
    private String email;

    @Column(name = "username", length = 50,unique = true,nullable = false,updatable = false)
    private String username;

    @Column(name = "password", length = 50,nullable = false)
    private String password;

}
