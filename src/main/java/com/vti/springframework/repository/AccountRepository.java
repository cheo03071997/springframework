package com.vti.springframework.repository;

import com.vti.springframework.modal.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    List<Account> findByUsernameContainsAndEmailContains(String username, String email);

    Page<Account> findByUsernameContains(String username, Pageable pageable);

    Optional<Account> findByUsername(String username);
}
