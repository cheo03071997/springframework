package com.vti.springframework.repository;

import com.vti.springframework.modal.entity.Department;
import com.vti.springframework.modal.entity.TypeDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department , Integer>, JpaSpecificationExecutor<Department> {
    @Modifying
    // Search department theo ten method
    List<Department> findAllByNameContainingAndTotalMemberBetweenAndTypeDepartmentEquals(String name, int minTotalMember, int maxTotalMember, TypeDepartment type);

    // cach 2.1: search theo department theo cau lenh HQL
    @Query(value = "SELECT dp FROM Department dp" +
            " WHERE dp.name like :name AND dp.totalMember BETWEEN :minTotalMember AND :maxTotalMember AND dp.typeDepartment = :type ")
    List<Department> searchV1(String name, int minTotalMember, int maxTotalMember, TypeDepartment type);

    // cach 2.2: search theo department theo cau lenh SQL (nativeQuery = true)
    @Query(value = "SELECT * FROM Department dp" +
            " WHERE dp.name like :name AND dp.total_member BETWEEN :minTotalMember AND :maxTotalMember AND dp.type = :type", nativeQuery = true)
    List<Department> searchV2(String name, int minTotalMember, int maxTotalMember, String type);

}
