package com.vti.springframework.repository.specification;

import com.vti.springframework.modal.dto.DepartmentRequest;
import com.vti.springframework.modal.entity.Department;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class DepartmentSpecification {
    public static Specification<Department> buildCondition(DepartmentRequest request){
        return Specification.where(findByName(request))
                .and(findByType(request));
    }

    private static Specification<Department> findByName(DepartmentRequest request){
        if(!StringUtils.isEmpty(request.getName())){
            return (root, query, cri) -> {
                return cri.like(cri.lower(root.get("name")), "%" + request.getName().toLowerCase() + "%" );
            };

        }
        return null;
    }

    private static Specification<Department> findByType(DepartmentRequest request){
        if(request.getTypeDepartment() !=null){
            return (root, query, cri) -> {
                return cri.equal(cri.lower(root.get("typeDepartment")), request.getTypeDepartment() );
            };

        }
        return null;
    }
}
