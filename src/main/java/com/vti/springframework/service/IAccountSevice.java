package com.vti.springframework.service;

import com.vti.springframework.modal.dto.AccountRequest;
import com.vti.springframework.modal.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface IAccountSevice {
    Page<Account> getAllAccount(AccountRequest request);

    Account getByID(int id);

    Account update(int id, Account account);

    void create(Account account);

    void delete(int id);

    List<Account> searchByName(String name,String email);
}
