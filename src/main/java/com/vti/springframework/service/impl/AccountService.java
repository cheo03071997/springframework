package com.vti.springframework.service.impl;

import com.vti.springframework.modal.dto.AccountRequest;
import com.vti.springframework.modal.entity.Account;
import com.vti.springframework.repository.AccountRepository;
import com.vti.springframework.service.IAccountSevice;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AccountService implements IAccountSevice {
    @Autowired
    private AccountRepository repository;

    @Override
    public Page<Account> getAllAccount(AccountRequest request) {
        Pageable pageable = Pageable.ofSize(request.getPageSize()).withPage(request.getPageNumber());
        PageRequest pageRequest;
        if("DESC".equals(request.getSortType())){
           pageRequest = PageRequest.of(request.getPageNumber(),request.getPageSize(), Sort.by(request.getSortBy()).descending());
        }else {
            pageRequest = PageRequest.of(request.getPageNumber(),request.getPageSize(), Sort.by(request.getSortBy()).ascending());
        }


        return repository.findByUsernameContains(request.getName() ,pageRequest);
    }

    @Override
    public Account getByID(int id) {
        Optional<Account> accountOptional = repository.findById(id);
        if (accountOptional.isEmpty()){
            return null;
        }else {
            return accountOptional.get();
        }
    }

    @Override
    public Account update(int id, Account account) {
        Account accountDb = getByID(id);
        if(accountDb != null){
            repository.saveAndFlush(account);
        }
        return null;
    }

    @Override
    public void create(Account account) {
        repository.save(account);
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    @Override
    public List<Account> searchByName(String name, String email) {
        return repository.findByUsernameContainsAndEmailContains(name, email);
    }


}
