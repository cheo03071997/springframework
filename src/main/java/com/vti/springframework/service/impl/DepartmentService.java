package com.vti.springframework.service.impl;

import com.vti.springframework.modal.dto.DepartmentRequest;
import com.vti.springframework.modal.entity.Department;
import com.vti.springframework.modal.entity.TypeDepartment;
import com.vti.springframework.repository.DepartmentRepository;
import com.vti.springframework.repository.specification.DepartmentSpecification;
import com.vti.springframework.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackOn = Exception.class)
public class DepartmentService implements IDepartmentService {
//    #Cach 1
//    @Autowired
//    private DepartmentRepository repository;

    private final DepartmentRepository repository;

    @Autowired
    public DepartmentService(DepartmentRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Department> getAll() {
        return repository.findAll();
    }

    @Override
    public List<Department> search(DepartmentRequest request) {
//        String name = "%"+ request.getName() +"%";
//        int min = request.getMinTotalMember();
//        int max = request.getMaxTotalMember();
//        TypeDepartment type = request.getTypeDepartment();
//        return repository.searchV2(name,min,max,type.name());

        Specification<Department> condition = DepartmentSpecification.buildCondition(request);
        return repository.findAll(condition);
    }

    @Override
    public Department getById(int id) {
        Optional<Department> optional = repository.findById(id);
        return optional.orElse(null);

    }

    @Override
    public void delete(int id) {
        if(repository.findById(id).isPresent()){
            repository.deleteById(id);
        }else {
            throw new RuntimeException("Department Khong ton tai!!!");
        }
    }
}
