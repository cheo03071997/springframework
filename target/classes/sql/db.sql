DROP DATABASE IF EXISTS JPA;
CREATE DATABASE JPA;

use JPA;

drop table if exists Account;
create table Account (
                         id	        		int auto_increment primary key,
                         username			varchar(50) unique,
                         email				varchar(50) unique not null,
                         password			varchar(50) not null
);


drop table if exists Department;
create table Department (
                            id	        		int auto_increment primary key,
                            name	    		varchar(50) not null unique,
                            totalMember			int,
                            type               enum('Dev','Test','ScrumMaster','PM'),
                            created_date			date
);

INSERT INTO JPA.`Account` (username, email, password) VALUES ('Nguyễn Văn A', 'a@gmail.com', '123456');
INSERT INTO JPA.`Account` (username, email, password) VALUES ('Nguyễn Văn B', 'b@gmail.com', '123456');
INSERT INTO JPA.`Account` (username, email, password) VALUES ('Nguyễn Văn C', 'c@gmail.com', '123456');

INSERT INTO JPA.`Department` (name, total_member, type, created_date) VALUES ('phòng ban 1', '6', 'dev', '2023-01-11 19:29:32');
INSERT INTO JPA.`Department` (name, total_member, type, created_date) VALUES ('phòng ban 2', '10', 'Test', '2023-01-11 19:29:32');
INSERT INTO JPA.`Department` (name, total_member, type, created_date) VALUES ('Phòng ban 3', '5', 'PM', '2023-01-11 19:29:32');